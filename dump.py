projsys = False
if projsys:
    mfac = spsla.splu(M)
    mijtl = []
    for kcol in range(J.shape[0]):
        curcol = np.array(J.T[:, kcol].todense())
        micc = np.array(mfac.solve(curcol.flatten()))
        mijtl.append(micc.reshape((NV, 1)))

    mijt = np.hstack(mijtl)
    si = np.linalg.inv(J*mijt)

    def _comp_proj(v):
        jtsijv = np.array(J.T*np.dot(si, J*v)).flatten()
        mjtsijv = mfac.solve(jtsijv)
        return v - mjtsijv.reshape((NV, 1))

    def _comp_proj_t(v):
        jtsijv = np.array(J.T*np.dot(si, J*v)).flatten()
        mjtsijv = mfac.solve(jtsijv)
        return v - mjtsijv.reshape((NV, 1))

    v = np.random.randn(A.shape[0], 1)
    pv = _comp_proj(v)

