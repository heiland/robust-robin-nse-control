robust-robin-nse-control
---

This Python module will provide routines that are designed to stabilize flows under regime changes by output-based feedback robustified against coprime factor perturbations of the underlying transfer function.

To check controllability of unstable subsystem of the discretization, adjust the parameters inside and run the script

```
python check_cntrlblt.py
```

You may need to export the matrices first by means of the script `exp_mats_script.py`.

Dependencies
------------
Apart from `numpy` and `scipy`, the code for the Navier-Stokes equations depends on the FEM suite [FEniCS](http://www.fenicsproject.org) and on several python modules that are listed for direct download in `get_upd_pymodules.sh`.

To download and import these modules run
```
source get_upd_pymodules.sh
```
in the console.

To get the meshs, run `source get_meshs.sh`.


