#!/bin/bash
# use this script by calling `source get-upd-gh-deps.sh` in the console
mkdir mesh
cd mesh
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_1.xml.gz-20100415150034-j7ci9o6bhb9ylp09-10/cylinder_1.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_2.xml.gz-20100415150034-j7ci9o6bhb9ylp09-11/cylinder_2.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_3.xml.gz-20100415150034-j7ci9o6bhb9ylp09-12/cylinder_3.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_4.xml.gz-20100415150034-j7ci9o6bhb9ylp09-14/cylinder_4.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_5.xml.gz-20100415150034-j7ci9o6bhb9ylp09-15/cylinder_5.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_6.xml.gz-20100415150034-j7ci9o6bhb9ylp09-16/cylinder_6.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_7.xml.gz-20100525201947-d32iglcmhbvtl10w-1/cylinder_7.xml.gz
wget http://bazaar.launchpad.net/~nsbench/nsbench/main/download/kvs%40bigblue-20100525211443-cwp9t33mc2z2cv48/cylinder_8.xml.gz-20100525201950-c57uh8sufda6fq12-1/cylinder_8.xml.gz
cd ..
